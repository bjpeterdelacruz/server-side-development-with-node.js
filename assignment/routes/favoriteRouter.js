var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Verify = require('./verify');
var ObjectId = require('mongoose').Types.ObjectId;

var Favorites = require('../models/favorites');

var favoriteRouter = express.Router();
favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')
.all(Verify.verifyOrdinaryUser)

.get(function(req, res, next) {
    Favorites.find({})
        .populate('dishes')
		.populate('favoritedBy')
        .exec(function(err, favorite) {
        if (err) {
            throw err;
        }
        res.json(favorite);
    });
})

.post(function(req, res, next) {
	var id = new ObjectId(req.decoded._doc._id);
	Favorites.find({ favoritedBy : id }, function(err, favorite) {
		if (err) {
			throw err;
		}
		if (!favorite) {
			return;
		}
		var fav = getFavorite(req, favorite);
		if (fav) {
			for (idx = 0; idx < fav.dishes.length; idx++) {
				if (fav.dishes[idx] == req.body._id) {
                    // Favorite already exists in list, so just return list of favorites.
					res.json(favorite);
					return;
				}
			}
            // Favorite not found in list, so add it and then save.
            fav.dishes.push({ _id : new ObjectId(req.body._id)});
            Favorites.findByIdAndUpdate(fav._id, fav, { new : true }, function(err, favorite) {
                if (err) {
                    throw err;
                }
				console.log("Successfully updated list of user's favorites.");
				res.json(favorite);
            });
		}
		else {
			// User has no favorites, so create new document and add favorite.
			var newFavorite = Favorites({
				dishes : [{ _id : new ObjectId(req.body._id) }],
				favoritedBy : new ObjectId(req.decoded._doc._id)
			});
			newFavorite.save(function(err) {
				if (err) {
					throw err;
				}
				console.log("Successfully created list of user's favorites.");
				res.json(newFavorite);
			});
		}
	});
})

.delete(function(req, res, next) {
	var id = new ObjectId(req.decoded._doc._id);
    Favorites.remove({ favoritedBy : id }, function(err, favorite) {
        if (err) {
            throw err;
        }
		res.writeHead(200, { 'Content-Type': 'text/plain' });
		res.end("Successfully deleted list of user's favorites!");
    });
});

favoriteRouter.route('/:dishObjectId')
.all(Verify.verifyOrdinaryUser)

.get(function(req, res, next) {
	var id = new ObjectId(req.decoded._doc._id);
    Favorites.find({ favoritedBy : id })
        .populate('dishes')
		.populate('favoritedBy')
        .exec(function(err, favorite) {
        if (err) {
            throw err;
        }
		if (!favorite) {
			return;
		}
		var fav = getFavoriteById(req, favorite);
		if (fav) {
			for (idx = 0; idx < fav.dishes.length; idx++) {
				if (fav.dishes[idx]._id == req.params.dishObjectId) {
                    res.json(fav.dishes[idx]);
					return;
				}
			}
		}
		res.writeHead(404, { 'Content-Type': 'text/plain' });
		res.end("User's favorite not found!");
    });
})

.delete(function(req, res, next) {
	var id = new ObjectId(req.decoded._doc._id);
    Favorites.find({ favoritedBy : id }, function(err, favorite) {
        if (err) {
            throw err;
        }
		if (!favorite) {
			return;
		}
		var fav = getFavorite(req, favorite);
		if (fav) {
			// Remove favorite from list, then save.
			for (idx = 0; idx < fav.dishes.length; idx++) {
				if (fav.dishes[idx] == req.params.dishObjectId) {
					fav.dishes.splice(idx, 1);
					break;
				}
			}
			Favorites.findByIdAndUpdate(fav._id, fav, { new : true }, function(err, favorite) {
				if (err) {
					throw err;
				}
				console.log("Successfully removed favorite from list.");
				res.json(favorite);
			});
		}
		else {
            res.writeHead(404, { 'Content-Type': 'text/plain' });
            res.end("User's favorites not found!");
		}
    });
});

function getFavorite(req, favorite) {
    for (idx = 0; idx < favorite.length; idx++) {
        if (favorite[idx].favoritedBy == req.decoded._doc._id) {
            return favorite[idx];
        }
    }
    return null;
}

function getFavoriteById(req, favorite) {
    for (idx = 0; idx < favorite.length; idx++) {
        if (favorite[idx].favoritedBy._id == req.decoded._doc._id) {
            return favorite[idx];
        }
    }
    return null;
}

module.exports = favoriteRouter;
